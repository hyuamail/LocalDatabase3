package id.sch.smktelkom_mlg.learn.localdatabase3.app;

import android.app.Application;

import org.greenrobot.greendao.database.Database;

import id.sch.smktelkom_mlg.learn.localdatabase3.model.DaoMaster;
import id.sch.smktelkom_mlg.learn.localdatabase3.model.DaoSession;


/**
 * Created by hyuam on 28/03/2017.
 */

public class MyApp extends Application
{
    private DaoSession daoSession;
    
    @Override
    public void onCreate()
    {
        super.onCreate();
        
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "places-db", null);
        Database db = helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
    }
    
    public DaoSession getDaoSession()
    {
        return daoSession;
    }
}
