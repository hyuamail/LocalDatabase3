package id.sch.smktelkom_mlg.learn.localdatabase3.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

/**
 * Created by hyuam on 12/10/2016.
 */

@Entity(generateConstructors = false)
public class Place
{
    @Id
    public Long id;
    public String judul;
    public String deskripsi;
    public String detail;
    public String lokasi;
    public String foto;
    
    public Place()
    {
    }
    
    public Place(String judul, String deskripsi, String detail, String lokasi,
                 String foto)
    {
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.detail = detail;
        this.lokasi = lokasi;
        this.foto = foto;
    }
    
    public Long getId()
    {
        return this.id;
    }
    
    public void setId(long id)
    {
        this.id = id;
    }
    
    public void setId(Long id)
    {
        this.id = id;
    }
    
    public String getJudul()
    {
        return this.judul;
    }
    
    public void setJudul(String judul)
    {
        this.judul = judul;
    }
    
    public String getDeskripsi()
    {
        return this.deskripsi;
    }
    
    public void setDeskripsi(String deskripsi)
    {
        this.deskripsi = deskripsi;
    }
    
    public String getDetail()
    {
        return this.detail;
    }
    
    public void setDetail(String detail)
    {
        this.detail = detail;
    }
    
    public String getLokasi()
    {
        return this.lokasi;
    }
    
    public void setLokasi(String lokasi)
    {
        this.lokasi = lokasi;
    }
    
    public String getFoto()
    {
        return this.foto;
    }
    
    public void setFoto(String foto)
    {
        this.foto = foto;
    }
    
}
